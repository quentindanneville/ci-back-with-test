export type skillProps = {
    name: string,
    category: number | null
}