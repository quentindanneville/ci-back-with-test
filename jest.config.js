module.exports = {
    testEnvironment: 'node',
    moduleFileExtensions: ['js', 'json', 'node', 'ts'],
    roots: ['<rootDir>/dist']
};